package botexecutor

import (
	"errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/application-settings/v2/settings"
	"gitlab.com/shadowy/sei/notifications/bot-executor/db"
	"strings"
)

const (
	CmdHelp         = "help"
	CmdStatus       = "status"
	CmdActivate     = "activate"
	CmdDeactivate   = "deactivate"
	CmdAuthenticate = "authenticate"
)

const help = `
help - це повідомлення
activate <email> - включити повідомлення
deactivate - выключити повідомлення
status - стан надсилання повідомлень
`

const ErrorCommandNotFound = "command_not_found"
const AnswerStatusActive = "Відправлення повідомлень включено"
const AnswerStatusDeactivate = "Відправлення повідомлень не включена"

type CommandFunction func(serviceID, userID string, params ...string) (string, error)
type CommandExecutor struct {
	repository *db.NotificationRepository
	command    map[string]CommandFunction
}

func CreateCommandExecutor(config *settings.Configuration) *CommandExecutor {
	log.Logger.Info().Msg("botexecutor.CreateCommandExecutor")
	result := new(CommandExecutor)
	result.repository = db.CreateNotificationRepository(config)
	result.init()
	return result
}

func (cmd *CommandExecutor) Execute(command, serviceID, userID string) (string, error) {
	log.Logger.Debug().Str("command", command).Str("serviceID", serviceID).Str("userID", userID).
		Msg("CommandExecutor.Execute")
	cmdParts := strings.Fields(command)
	if len(cmdParts) == 0 {
		return "", errors.New("command is empty")
	}
	cmdName := strings.ToLower(cmdParts[0])
	if fun, ok := cmd.command[cmdName]; ok {
		return fun(serviceID, userID, cmdParts[1:]...)
	}
	return "", CreateError(ErrorCommandNotFound, "command not found")
}

func (cmd *CommandExecutor) IsAuthenticated(serviceID, userID string) (bool, error) {
	log.Logger.Debug().Str("serviceID", serviceID).Str("userID", userID).Msg("CommandExecutor.IsAuthenticated")
	return cmd.repository.IsAuthenticated(userID, serviceID)
}

func (cmd *CommandExecutor) IsActive(serviceID, userID string) (bool, error) {
	log.Logger.Debug().Str("serviceID", serviceID).Str("userID", userID).Msg("CommandExecutor.IsActive")
	return cmd.repository.GetStatus(userID, serviceID)
}

func (cmd *CommandExecutor) CommandList(serviceID, userID string) (list []string, err error) {
	log.Logger.Debug().Str("serviceID", serviceID).Str("userID", userID).Msg("CommandExecutor.CommandList")
	flag, err := cmd.IsAuthenticated(serviceID, userID)
	if err != nil {
		return nil, err
	}
	if !flag {
		return []string{CmdAuthenticate, CmdHelp}, nil
	}
	flag, err = cmd.IsActive(serviceID, userID)
	if err != nil {
		return nil, err
	}
	if flag {
		return []string{CmdStatus, CmdDeactivate, CmdHelp}, nil
	}
	return []string{CmdStatus, CmdActivate, CmdHelp}, nil
}

func (cmd *CommandExecutor) Link(serviceID, userID, email, phone string, data interface{}) error {
	log.Logger.Debug().
		Str("serviceID", serviceID).
		Str("userID", userID).
		Str("email", email).
		Str("phone", phone).
		Msg("CommandExecutor.Link")
	return cmd.repository.Link(userID, serviceID, email, phone, data)
}

func (cmd *CommandExecutor) activate(serviceID, userID string, params ...string) (string, error) {
	log.Logger.Debug().Str("serviceID", serviceID).Str("userID", userID).Str("params", strings.Join(params, ", ")).
		Msg("CommandExecutor.activate")
	err := cmd.repository.ActivateService(userID, serviceID)
	if err != nil {
		return "", err
	}
	return "ok", nil
}

func (cmd *CommandExecutor) deactivate(serviceID, userID string, params ...string) (string, error) {
	log.Logger.Debug().Str("serviceID", serviceID).Str("userID", userID).Str("params", strings.Join(params, ", ")).
		Msg("CommandExecutor.deactivate")
	err := cmd.repository.DeactivateService(userID, serviceID)
	if err != nil {
		return "", err
	}
	return "ok", nil
}

func (cmd *CommandExecutor) status(serviceID, userID string, params ...string) (string, error) {
	log.Logger.Debug().
		Str("serviceID", serviceID).
		Str("userID", userID).
		Str("params", strings.Join(params, ", ")).
		Msg("CommandExecutor.status")
	res, err := cmd.repository.GetStatus(userID, serviceID)
	if err != nil {
		return "", err
	}
	if res {
		return AnswerStatusActive, nil
	}
	return AnswerStatusDeactivate, nil
}

func (cmd *CommandExecutor) help(serviceID, userID string, params ...string) (string, error) {
	log.Logger.Debug().Str("serviceID", serviceID).Str("userID", userID).Str("params", strings.Join(params, ", ")).
		Msg("CommandExecutor.help")
	return help, nil
}

func (cmd *CommandExecutor) init() {
	cmd.command = make(map[string]CommandFunction)
	cmd.command[CmdActivate] = cmd.activate
	cmd.command[CmdDeactivate] = cmd.deactivate
	cmd.command[CmdStatus] = cmd.status
	cmd.command[CmdHelp] = cmd.help
}
