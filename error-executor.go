package botexecutor

type ErrorExecutor struct {
	Code string
	Text string
}

func (err *ErrorExecutor) Error() string {
	return err.Code + " - " + err.Text
}

func CreateError(code, text string) *ErrorExecutor {
	return &ErrorExecutor{
		Code: code,
		Text: text,
	}
}
