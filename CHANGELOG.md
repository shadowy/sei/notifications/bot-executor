# CHANGELOG

<!--- next entry here -->

## 0.4.3
2021-05-21

### Fixes

- update link method in repository (517b8c7e4f0a243d117fc7524961797c4e2469d0)

## 0.4.2
2021-05-21

### Fixes

- fix commands list (23767e63018402750c7ac79f94325ad4870a0079)

## 0.4.1
2021-05-21

### Fixes

- cmd to Cmd for commands const (89fc4b7e5090788155cc8340c87801973bb596bf)

## 0.4.0
2021-05-21

### Features

- change commands list and provide helper function to the next level (4d803de5816c5414f026e63c8556b5fab179cbf8)

## 0.3.2
2021-05-14

### Fixes

- update lib (5f8d6f7bfdd8c9a417c5a08c2b498abbf1c20da2)

## 0.3.1
2021-05-14

### Fixes

- fix sql for status (7112e0f6fae6bc9d607be53da0c4c5cfed6c8445)

## 0.3.0
2021-05-14

### Features

- add additional logics (7727c27ce371004e11a0a43d32c54a489205393b)

## 0.2.0
2021-05-14

### Features

- add custom error (ccbb8a6d9d95507a247840ca1a78665eb0f8dce3)

## 0.1.5
2021-05-13

### Fixes

- fix a small issue (ce51e559d3e8c469b6b09d165944ad2c5381ac50)

## 0.1.4
2021-05-13

### Fixes

- fix executor initializer (a8414fcc3129fc93a41a04241ebb6a6dc99deadb)

## 0.1.3
2021-05-11

### Fixes

- fix a small issue (3bb29d9571edd85d3527d06430c8bc2c8b40b921)

## 0.1.2
2021-05-11

### Fixes

- fix a small issue (bdfcad05da05724c0f05b0f17cbea30546821601)
- fix a small issue (f86cca5506cb2b40befd571967ff59747bbf2bd3)
- fix a small issue (4d84da234f4621043081c221a8e182016d81b1bb)

## 0.1.1
2021-05-11

### Fixes

- fix a small issue (78207a2c3dbe9b689af0d3ebf7c423fc82704bca)

## 0.1.0
2021-05-11

### Features

- initial commit (03038de29d5fcaa74097dacfd8f3b9df757841f8)

