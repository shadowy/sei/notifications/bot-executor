package db

import (
	"database/sql"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/application-settings/v2/settings"
	"gitlab.com/shadowy/sei/common/go-db/v2"
	"gitlab.com/shadowy/sei/notifications/bot-executor/model"
	"reflect"
)

const sqlNotificationGetIsAuthenticated = "select notification.is_client_service_authenticated($1, $2) as res"
const sqlNotificationLink = "select notification.link_client_service($1, $2, $3, $4, $5) as res"
const sqlNotificationGetStatus = "select notification.get_service_state($1, $2) as res"
const sqlNotificationSetServiceState = "select notification.set_service_state($1, $2, $3) as res"

type NotificationRepository struct {
	db.CommonRepository
}

func CreateNotificationRepository(config *settings.Configuration) *NotificationRepository {
	log.Logger.Info().Msg("botexecutor.db.CreateNotificationRepository")
	result := new(NotificationRepository)
	result.Init(config)
	return result
}

func (rep *NotificationRepository) IsAuthenticated(userID, serviceID string) (res bool, err error) {
	l := log.Logger.With().Str("userID", userID).Str("serviceID", serviceID).Logger()
	l.Debug().Msg("NotificationRepository.IsAuthenticated")
	client := rep.GetDB()
	row, err := client.GetSingleRowModel(sqlNotificationGetIsAuthenticated, reflect.TypeOf(model.SimpleBool{}), userID, serviceID)
	if err != nil {
		l.Error().Err(err).Stack().Msg("NotificationRepository.IsAuthenticated execute")
		return false, err
	}
	return row.(model.SimpleBool).Res, nil
}

func (rep *NotificationRepository) Link(userID, serviceID, email, phone string, data interface{}) error {
	l := log.Logger.With().
		Str("userID", userID).
		Str("serviceID", serviceID).
		Str("email", email).
		Str("phone", phone).
		Logger()
	l.Debug().Msg("NotificationRepository.Link")
	client := rep.GetDB()
	err := client.UsingWithTransaction(func(tx *sql.Tx) error {
		errSQL := client.ExecuteQuery(sqlNotificationLink, tx, userID, serviceID, email, phone, toJSON(data))
		if errSQL != nil {
			l.Error().Err(errSQL).Stack().Msg("NotificationRepository.SetData execute")
		}
		return errSQL
	})
	return err
}

func (rep *NotificationRepository) GetStatus(userID, serviceID string) (res bool, err error) {
	l := log.Logger.With().Str("userID", userID).Str("serviceID", serviceID).Logger()
	l.Debug().Msg("NotificationRepository.GetStatus")
	client := rep.GetDB()
	row, err := client.GetSingleRowModel(sqlNotificationGetStatus, reflect.TypeOf(model.SimpleBool{}), userID, serviceID)
	if err != nil {
		l.Error().Err(err).Stack().Msg("NotificationRepository.GetStatus execute")
		return false, err
	}
	return row.(model.SimpleBool).Res, nil
}

func (rep *NotificationRepository) ActivateService(clientID, serviceID string) error {
	log.Logger.Debug().Str("clientID", clientID).Str("serviceID", serviceID).Msg("NotificationRepository.ActivateService")
	return rep.setServiceState(clientID, serviceID, true)
}

func (rep *NotificationRepository) DeactivateService(clientID, serviceID string) error {
	log.Logger.Debug().Str("clientID", clientID).Str("serviceID", serviceID).Msg("NotificationRepository.DeactivateService")
	return rep.setServiceState(clientID, serviceID, false)
}

func (rep *NotificationRepository) setServiceState(clientID, serviceID string, state bool) error {
	l := log.Logger.With().Str("clientID", clientID).Str("serviceID", serviceID).Bool("state", state).Logger()
	l.Debug().Msg("NotificationRepository.setServiceState")
	client := rep.GetDB()
	err := client.UsingWithTransaction(func(tx *sql.Tx) error {
		errSQL := client.ExecuteQuery(sqlNotificationSetServiceState, tx, clientID, serviceID, state)
		if errSQL != nil {
			l.Error().Err(errSQL).Stack().Msg("NotificationRepository.setServiceState execute")
		}
		return errSQL
	})
	return err
}

func toJSON(search interface{}) string {
	jsonData, _ := json.Marshal(search)
	return string(jsonData)
}
