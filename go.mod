module gitlab.com/shadowy/sei/notifications/bot-executor

go 1.16

require (
	github.com/rs/zerolog v1.21.0
	gitlab.com/shadowy/go/application-settings/v2 v2.0.2
	gitlab.com/shadowy/sei/common/go-db/v2 v2.0.0
)
